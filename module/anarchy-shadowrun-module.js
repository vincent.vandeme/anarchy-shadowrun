
export class AnarchyShadowrunModule {
  static start() {
    const anarchyShadowrunModule = new AnarchyShadowrunModule();
    Hooks.once('init', async() => await anarchyShadowrunModule.onInit());
  }

  async onInit() {
    game.modules.anarchyShadowrunModule = this;
    Hooks.once('anarchy-loadStyles', styles => styles['style-deepnight'] = 'Deep night');
  }
}